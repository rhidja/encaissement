<?php 

function encaissement_form($form, &$form_state){
	
	$form['client'] = array(
		'#title'  => t('Client'),
		'#type'   => 'textfield',
		'#autocomplete_path' => 'client_autocomplete',
		'#required' => TRUE,
		);

	$form['d_arrivee'] = array(
		'#title' => t('Date d\'arrivée'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => date("Y:m:d"),
		'#prefix' => t('<div class="row"><div = class="col-md-3">'),
		'#suffix' => t('</div>'),		
		);
	
	$d = date("d")+1;

	$form['d_depart'] = array(
		'#title' => t('Date de départ'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => date("Y").':'.date("m").':'.$d,
		'#prefix' => t('<div = class="col-md-3">'),
		'#suffix' => t('</div>'),		
		);	

	$form['d_encaissement'] = array(
		'#title' => t('Date encaiss'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => date("Y:m:d"),
		'#prefix' => t('<div = class="col-md-3">'),
		'#suffix' => t('</div>'),		
		);

	$form['autres'] = array(
		'#title' => t('Autres'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#prefix' => t('<div = class="col-md-3">'),
		'#suffix' => t('</div></div>'),		
		);	

	$form['chambres'] = array(
		'#title' => t('Chambres'),
		'#type' => 'textfield',
		'#prefix' => t('<div class="row"><div = class="col-md-6">'),
		'#suffix' => t('</div>'),		
		);

	$form['nbrnuit'] = array(
		'#title' => t('Nbr Nuit'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div>'),		
		);

	$form['nbrtax'] = array(
		'#title' => t('Nbr Tax'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div>'),		
		);

	$form['nbrpdj'] = array(
		'#title' => t('Nbr PDJ'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div></div>'),			
		);

	$form['payements'] = array(
		'#type' => 'fieldset',
		);


	$form['payements']['cb'] = array(
		'#title' => t('CB'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#attributes' => array('class'=>array('calcul-total')),
		'#prefix' => t('<div class="row"><div = class="col-md-2">'),
		'#suffix' => t('</div>'),			
		);

	$form['payements']['esp'] = array(
		'#title' => t('ESP'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#attributes' => array('class'=>array('calcul-total')),
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div>'),		
		);

	$form['payements']['chq'] = array(
		'#title' => t('CHQ'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#attributes' => array('class'=>array('calcul-total')),
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div>'),		
		);

	$form['payements']['chv'] = array(
		'#title' => t('CHV'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#attributes' => array('class'=>array('calcul-total')),
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div>'),		
		);

	$form['payements']['vir'] = array(
		'#title' => t('VIR'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#default_value' => 0,
		'#attributes' => array('class'=>array('calcul-total')),
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div>'),			
		);

	$form['payements']['total'] = array(
		'#title' => t('Total'),
		'#type' => 'textfield',
		'#maxlength' => 10,
		'#prefix' => t('<div = class="col-md-2">'),
		'#suffix' => t('</div></div>'),			
		);

	$form['comment'] = array(
		'#title' => t('Commentaire'),
		'#type'  => 'textarea',
		'#rows'  => 1,
		);


	// Get the path to the module
	$path = drupal_get_path('module', 'encaissemnt');
	// Attach the CSS and JS to the form
	$form['#attached'] = array(
		'js' => array(
			'type' => 'file',
			'data' => $path . '/encaissemnt.js',
			),
		);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Enregistrer',
		'#attributes' => array('class' => array('btn btn-primary pull-right')),	
		);

	return $form;
}


function encaissement_form_validate($form, &$form_state) {
    //$year_of_birth = $form_state['values']['year_of_birth'];
    //if ($year_of_birth && ($year_of_birth < 1900 || $year_of_birth > 2000)) {
        //form_set_error('year_of_birth', 'Enter a year between 1900 and 2000.');
    //}
}

function encaissement_form_submit($form, &$form_state){

	db_insert('tb_encaissement')
	->fields(array(
		'client'   	=> $form_state['values']['client'],
		'chambres' 	=> $form_state['values']['chambres'],
		'nbrnuit'  	=> $form_state['values']['nbrnuit'],
		'nbrtax'   	=> $form_state['values']['nbrtax'],
		'nbrpdj'   	=> $form_state['values']['nbrpdj'],
		'cb'    	=> $form_state['values']['cb'],
		'esp'   	=> $form_state['values']['esp'],
		'chq'   	=> $form_state['values']['chq'],
		'chv'   	=> $form_state['values']['chv'],
		'vir'   	=> $form_state['values']['vir'],
		'comment' 	=> $form_state['values']['comment'],
		'd_arr'    	=> $form_state['values']['d_arrivee'],
		'd_dep'    	=> $form_state['values']['d_depart'],
		'd_enc'    	=> $form_state['values']['d_encaissement'],
		'd_cre'    	=> date("Y-m-d H:i:s"),
		))
	->execute();

	//$form_state['redirect'] = 'nouveau_sejour';

	drupal_set_message(t('The form has been submitted.'));
}


function client_autocomplete_callback($string) {
	$matches = array();

  // Some fantasy DB table which holds cities
	$query = db_select('tb_encaissement', 'en');

  // Select rows that match the string
	$return = $query
	->fields('en', array('client'))
	->condition('en.client', '%' . db_like($string) . '%', 'LIKE')
	->range(0, 10)
	->execute();

  // add matches to $matches  
	foreach ($return as $row) {
		$matches[$row->client] = check_plain($row->client);
	}

  // return for JS
	drupal_json_output($matches);
}